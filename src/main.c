#define _DEFAULT_SOURCE
#include "mem.h"
#include "mem_internals.h"
#include <sys/mman.h>

#define HEAP_SIZE 1000
enum tests {
    TEST1 = 1,
    TEST2,
    TEST3,
    TEST4,
    TEST5,
};

static void *init_test(const char *test_name, size_t heap_size) {
    printf("%s:\n", test_name);
    void *heap = heap_init(heap_size);
    debug_heap(stdout, heap);
    return heap;
}

static void *allocate_block(void *heap, size_t block_size) {
    void *block = _malloc(block_size);
    debug_heap(stdout, heap);
    return block;
}

static void free_block(void *heap, void *block) {
    _free(block);
    debug_heap(stdout, heap);
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*)((uint8_t*)contents - offsetof(struct block_header, contents));
}

static int test_1() {
    void *heap = init_test("№1 Memory allocation", HEAP_SIZE);
    void *block = allocate_block(heap, REGION_MIN_SIZE);
    if (block == NULL) {
        fprintf(stderr, "Allocation error!");
        return TEST1;
    }
    free_block(heap, block);
    if (!block_get_header(block)->is_free) {
        fprintf(stderr, "Free error!");
        return TEST1;
    }
    heap_term();
    return 0;
}

static int test_2() {
    void *heap = init_test("№2 Free 1 block", HEAP_SIZE);
    void *block_1 = allocate_block(heap, REGION_MIN_SIZE);
    void *block_2 = allocate_block(heap, REGION_MIN_SIZE);
    if (block_1 == NULL || block_2 == NULL) {
        fprintf(stderr, "Allocation error!");
        return TEST2;
    }
    free_block(heap, block_1);
    if (!block_get_header(block_1)->is_free) {
        fprintf(stderr, "Free error!");
        return TEST2;
    }
    if (block_get_header(block_2)->is_free) {
        fprintf(stderr, "Free error!");
        return TEST2;
    }
    free_block(heap, block_2);
    if (!block_get_header(block_2)->is_free) {
        fprintf(stderr, "Free error!");
        return TEST2;
    }
    heap_term();
    return 0;
}


static int test_3() {
    void *heap = init_test("№3 Free 2 block", HEAP_SIZE);
    void *block_1 = allocate_block(heap, REGION_MIN_SIZE);
    void *block_2 = allocate_block(heap, REGION_MIN_SIZE);
    void *block_3 = allocate_block(heap, REGION_MIN_SIZE);
    if (block_1 == NULL || block_2 == NULL || block_3 == NULL) {
        fprintf(stderr, "Allocation error!");
        return TEST3;
    }
    free_block(heap, block_2);
    if (!block_get_header(block_2)->is_free) {
        fprintf(stderr, "Free error!");
        return TEST3;
    }
    if (block_get_header(block_1)->is_free || block_get_header(block_3)->is_free) {
        fprintf(stderr, "Free error!");
        return TEST3;
    }
    free_block(heap, block_1);
    free_block(heap, block_3);
    if (!block_get_header(block_1)->is_free || !block_get_header(block_3)->is_free) {
        fprintf(stderr, "Free error!");
        return TEST3;
    }
    heap_term();
    return 0;
}


static int test_4() {
    void *heap = init_test("№4 The new memory region expands the old", HEAP_SIZE);
    void *block_1 = allocate_block(heap, HEAP_SIZE);
    if (block_1 == NULL) {
        fprintf(stderr, "Allocation error!");
        return TEST1;
    }
    if (block_get_header(block_1)->is_free) {
        fprintf(stderr, "Free error!");
        return TEST4;
    }
    void *block_2 = allocate_block(heap, REGION_MIN_SIZE);
    if (block_get_header(block_2)->is_free) {
        fprintf(stderr, "Free error!");
        return TEST4;
    }

    free_block(heap, block_1);
    if (!block_get_header(block_1)->is_free) {
        fprintf(stderr, "Free error!");
        return TEST4;
    }
    free_block(heap, block_2);
    if (!block_get_header(block_2)->is_free) {
        fprintf(stderr, "Free error!");
        return TEST4;
    }
    heap_term();
    return 0;
}

static int test_5() {
    void *heap = init_test(
            "№5 The new region is allocated in other place",
            HEAP_SIZE);
    void *addr = mmap(heap, HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS, -1, 0);
    if (addr == MAP_FAILED) {
        fprintf(stderr, "Map failed!");
        return TEST5;
    }
    debug_heap(stdout, heap);
    void *block = allocate_block(heap, REGION_MIN_SIZE);
    if (block == NULL) {
        fprintf(stderr, "Allocation error!");
        return TEST5;
    }
    free_block(heap, block);
    if (!block_get_header(block)->is_free) {
        fprintf(stderr, "Free error!");
        return TEST5;
    }
    heap_term();
    return 0;
}


int main() {
    int tests[] = {test_1(), test_2(), test_3(), test_4(), test_5()};
    for (int i = 0; i < 5; i++) {
        if (tests[i] != 0) {
            fprintf(stderr, "Failed in test №%d", i + 1);
            return 1;
        }
    }
    printf("All tests passed.\n");
}


